import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, Col, Form, FormGroup, PageHeader} from "react-bootstrap";

import {registerUser} from "../../../store/actions/users";
import FormElement from "../../../components/UI/Form/FormElement/FormElement";

class Register extends Component {
  state = {
    username: '',
    password: '',
    display_name: '',
    phone_number: ''
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  submitFormHandler = event => {
    event.preventDefault();

    this.props.registerUser(this.state);
  };

  fieldHasError = fieldName => {
    return this.props.error && this.props.error.errors[fieldName];
  };

  render() {
    return (
      <Fragment>
        <PageHeader>Register new user</PageHeader>
        <Form horizontal onSubmit={this.submitFormHandler}>

          <FormElement
            propertyName="username"
            title="Username"
            placeholder="Enter username"
            type="text"
            value={this.state.username}
            changeHandler={this.inputChangeHandler}
            autoComplete="new-username"
            error={this.fieldHasError('username') && this.props.error.errors.username.message}
          />

          <FormElement
            propertyName="password"
            title="Password"
            placeholder="Enter password"
            type="password"
            value={this.state.password}
            changeHandler={this.inputChangeHandler}
            autoComplete="new-password"
            error={this.fieldHasError('password') && this.props.error.errors.password.message}
          />

          <FormElement
            propertyName="display_name"
            title="Display name"
            placeholder="Enter your name"
            type="text"
            value={this.state.display_name}
            changeHandler={this.inputChangeHandler}
            autoComplete="new-display-name"
            error={this.fieldHasError('display_name') && this.props.error.errors.display_name.message}
          />

          <FormElement
            propertyName="phone_number"
            title="Phone number"
            placeholder="Enter your phone number"
            type="tel"
            value={this.state.phone_number}
            changeHandler={this.inputChangeHandler}
            autoComplete="new-phone-number"
            error={this.fieldHasError('phone_number') && this.props.error.errors.phone_number.message}
          />

          <FormGroup>
            <Col smOffset={2} sm={10}>
              <Button
                bsStyle="primary"
                type="submit"
              >Register</Button>
            </Col>
          </FormGroup>
        </Form>
      </Fragment>
    )
  }
}

const mapStateToProps = state => ({
  error: state.users.registerError
});

const mapDispatchToProps = dispatch => ({
  registerUser: userData => dispatch(registerUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);