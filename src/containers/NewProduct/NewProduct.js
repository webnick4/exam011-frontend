import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { PageHeader } from "react-bootstrap";


import {createProduct, fetchCategories} from "../../store/actions/products";
import ProductForm from "../../components/ProductForm/ProductForm";

class NewProduct extends Component {
  createProduct = productData => {
    this.props.onProductCreated(productData).then(() => {
      this.props.history.push('/');
    });
  };

  componentDidMount() {
    this.props.onFetchCategories();
  }


  render() {
    return (
      <Fragment>
        <PageHeader>Create new item</PageHeader>
        <ProductForm onSubmin={this.createProduct} categories={this.props.categories} />
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    categories: state.categories.categories
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onProductCreated: productData => dispatch(createProduct(productData)),
    onFetchCategories: () => dispatch(fetchCategories())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(NewProduct);