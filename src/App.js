import React, { Component } from 'react';

import Layout from "./containers/Layout/Layout";
import {Route, Switch} from "react-router-dom";

import FullItems from "./containers/FullItems/FullItems";
import Register from "./containers/Users/Register/Register";
import Login from "./containers/Users/Login/Login";
import NewProduct from "./containers/NewProduct/NewProduct";

class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route path="/" exact component={FullItems}/>
          <Route path="/register" exact component={Register}/>
          <Route path="/login" exact component={Login}/>
          <Route path="/products/new-item" exact component={NewProduct}/>
        </Switch>
      </Layout>
    );
  }
}

export default App;
