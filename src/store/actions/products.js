import axios from '../../axios-api';
import {CREATE_PRODUCT_SUCCESS, FETCH_CATEGORIES_SUCCESS} from "./actionTypes";

export const fetchCategoriesSuccess = categories => {
  return { type: FETCH_CATEGORIES_SUCCESS, categories };
};

export const fetchCategories = () => {
  return dispatch => {
    return axios.get('/categories').then(
      response => dispatch(fetchCategoriesSuccess(response.data))
    );
  };
};

export const createProductSuccess = () => {
  return { type: CREATE_PRODUCT_SUCCESS };
};

export const createProduct = (productData) => {
  return dispatch => {
    return axios.post('/products', productData).then(
      response => dispatch(createProductSuccess())
    );
  };
};