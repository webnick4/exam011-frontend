import React, { Component } from 'react';
import { Button, Col, ControlLabel, Form, FormControl, FormGroup } from "react-bootstrap";

class ProductForm extends Component {
  state = {
    title: '',
    description: '',
    image: '',
    category: ''
  };

  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();
    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });

    this.props.onSubmin(formData);
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  fileChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.files[0]
    });
  };


  render() {
    return (
      <Form horizontal onSubmit={this.submitFormHandler}>
        <FormGroup controlId="productTitle">
          <Col componentClass={ControlLabel} sm={2}>
            Title
          </Col>
          <Col sm={10}>
            <FormControl
              type="text" required
              placeholder="Enter product title"
              name="title"
              value={this.state.title}
              onChange={this.inputChangeHandler}
            />
          </Col>
        </FormGroup>

        <FormGroup controlId="productDescription">
          <Col componentClass={ControlLabel} sm={2}>
            Description
          </Col>
          <Col sm={10}>
            <FormControl
              componentClass="textarea"
              placeholder="Enter description"
              name="description"
              value={this.state.description}
              onChange={this.inputChangeHandler}
            />
          </Col>
        </FormGroup>

        <FormGroup controlId="productImage">
          <Col componentClass={ControlLabel} sm={2}>
            Image
          </Col>
          <Col sm={10}>
            <FormControl
              placeholder="Add file"
              type="file"
              name="image"
              onChange={this.fileChangeHandler}
            />
          </Col>
        </FormGroup>

        <FormGroup controlId="productCategory">
          <ControlLabel>Category</ControlLabel>
          <FormControl componentClass="select" placeholder="Select category...">
            {
              this.props.categories.map(category => {
                console.log(category);
                return <option
                  key={category._id}
                  name="category"
                  value={category.title.toLowerCase()}
                >{category.title}
                </option>
              })
            }
          </FormControl>
        </FormGroup>

        <FormGroup>
          <Col smOffset={2} sm={10}>
            <Button bsStyle="primary" type="submit">Create item</Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}

export default ProductForm;