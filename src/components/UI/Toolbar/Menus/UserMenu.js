import React, {Fragment} from 'react';
import {LinkContainer} from "react-router-bootstrap";
import {MenuItem, Nav, NavDropdown} from "react-bootstrap";

const UserMenu = ({user, logout}) => {
  const navTitle = (
    <Fragment>
      Hello, <b>{user.display_name}</b>!
    </Fragment>
  );

  return (
    <Nav pullRight>
      <NavDropdown title={navTitle} id="user-menu">
        <LinkContainer to="/new-item" exact>
          <MenuItem>Add new item</MenuItem>
        </LinkContainer>
        <MenuItem divider/>
        <MenuItem onClick={logout}>Logout</MenuItem>
      </NavDropdown>
    </Nav>
  )
};

export default UserMenu;